var searchData=
[
  ['cellrange',['CellRange',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html',1,'SpreadsheetMLWrapper']]],
  ['cellrange',['CellRange',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#a25a82153efa9223b39efb4ecd8710bce',1,'SpreadsheetMLWrapper.CellRange.CellRange(string cellRange)'],['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#acc89fcdcb1c72fc4b30a9af77a65c642',1,'SpreadsheetMLWrapper.CellRange.CellRange(string cellAddressStart, string cellAddressEnd)']]],
  ['cellrange_2ecs',['CellRange.cs',['../_cell_range_8cs.html',1,'']]],
  ['celltype',['CellType',['../namespace_spreadsheet_m_l_wrapper.html#a59d5d94477b2832dee304505733f99fb',1,'SpreadsheetMLWrapper']]],
  ['center',['Center',['../namespace_spreadsheet_m_l_wrapper.html#a17dee88f159c8bcd692b2189c5cd0a7ba4f1f6016fc9f3f2353c0cc7c67b292bd',1,'SpreadsheetMLWrapper']]],
  ['close',['Close',['../class_spreadsheet_m_l_wrapper_1_1_spreadsheet_update_tool.html#ac9221b313f8086e8364b950579f1d7c5',1,'SpreadsheetMLWrapper::SpreadsheetUpdateTool']]],
  ['columnend',['ColumnEnd',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#adf630d61a2369d4184d7ead85ccf2d3c',1,'SpreadsheetMLWrapper::CellRange']]],
  ['columnnameend',['ColumnNameEnd',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#aa51b7331cc933f7a9c67c2bcb60437c2',1,'SpreadsheetMLWrapper::CellRange']]],
  ['columnnamestart',['ColumnNameStart',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#ae69a7181f8489c1d78e983afa5284cee',1,'SpreadsheetMLWrapper::CellRange']]],
  ['columnstart',['ColumnStart',['../class_spreadsheet_m_l_wrapper_1_1_cell_range.html#ac49c5049763ebdaaeba1f38da7b86ffe',1,'SpreadsheetMLWrapper::CellRange']]],
  ['currentpagenumber',['CurrentPageNumber',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea35d3477d5db091c77cb472098c028a25',1,'SpreadsheetMLWrapper']]]
];
