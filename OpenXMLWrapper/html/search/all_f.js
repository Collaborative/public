var searchData=
[
  ['textbold',['TextBold',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea0d386a3565827d67f4720e88bf7d387e',1,'SpreadsheetMLWrapper']]],
  ['textbolditalic',['TextBoldItalic',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea048ca5b220b60ebd311af8b805573938',1,'SpreadsheetMLWrapper']]],
  ['textdoubleunderline',['TextDoubleUnderline',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeac201918140832a9376a00c66c6c7ec5a',1,'SpreadsheetMLWrapper']]],
  ['textfontcolor',['TextFontColor',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea60670c2ca2038032e256f4685d64d528',1,'SpreadsheetMLWrapper']]],
  ['textitalic',['TextItalic',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea4b87d00c9357797e704976c09fc34555',1,'SpreadsheetMLWrapper']]],
  ['textoutlinestyle',['TextOutlineStyle',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeac44987e44e2ddbe72467f068456c217d',1,'SpreadsheetMLWrapper']]],
  ['textregular',['TextRegular',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeacd449fed2e5b62fadff33a6329e8f5aa',1,'SpreadsheetMLWrapper']]],
  ['textshadow',['TextShadow',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeac4acdba3a12febf8d37b296b640549fc',1,'SpreadsheetMLWrapper']]],
  ['textsingleunderline',['TextSingleUnderline',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeae8b24be170619b3115a677cf667b5f14',1,'SpreadsheetMLWrapper']]],
  ['textstrikethrough',['TextStrikethrough',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea56459fa1256f2bc1fe72dad47ee047b1',1,'SpreadsheetMLWrapper']]],
  ['textsubscript',['TextSubscript',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea92c157944f6a4a00f7cbe34f9005a872',1,'SpreadsheetMLWrapper']]],
  ['textsuperscript',['TextSuperscript',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffea9e23a1a8e8bb6dd01db014d68b59c752',1,'SpreadsheetMLWrapper']]],
  ['time',['Time',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeaa76d4ef5f3f6a672bbfab2865563e530',1,'SpreadsheetMLWrapper']]],
  ['totalpages',['TotalPages',['../namespace_spreadsheet_m_l_wrapper.html#a6f75704b6346f68020cca17746cd9ffeafac093ddb7dab27585ae130bc39886d8',1,'SpreadsheetMLWrapper']]]
];
