var _spreadsheet_update_tool_8cs =
[
    [ "SpreadsheetUpdateTool", "class_spreadsheet_m_l_wrapper_1_1_spreadsheet_update_tool.html", "class_spreadsheet_m_l_wrapper_1_1_spreadsheet_update_tool" ],
    [ "A", "_spreadsheet_update_tool_8cs.html#aa23a4a81e14f34cd2a02a2c35ee2583e", null ],
    [ "A14", "_spreadsheet_update_tool_8cs.html#affca6673be2ee761a8b1adaa6f07e090", null ],
    [ "Xdr", "_spreadsheet_update_tool_8cs.html#abc29a8a375a62dca2bbe913bb59975ed", null ],
    [ "CellType", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fb", [
      [ "Boolean", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fba27226c864bac7454a8504f8edb15d95b", null ],
      [ "Number", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fbab2ee912b91d69b435159c7c3f6df7f5f", null ],
      [ "Error", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fba902b0d55fddef6f8d651fe1035b7d4bd", null ],
      [ "SharedString", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fbaed142b1703a65152d8094ae2867dae36", null ],
      [ "String", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fba27118326006d3829667a400ad23d5d98", null ],
      [ "InlineString", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fba750b83dd089f4c47efed9dd92143de7f", null ],
      [ "Date", "_spreadsheet_update_tool_8cs.html#a59d5d94477b2832dee304505733f99fba44749712dbec183e983dcd78a7736c41", null ]
    ] ],
    [ "HFItem", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483", [
      [ "OddHeader", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483ab5cfcc9727d6f0f548a79fadcfd8a0ef", null ],
      [ "EvenHeader", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483aebb7836f654d05cb798e0b5564a6d9da", null ],
      [ "FirstHeader", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483a001df7e0202f79d6bbd070b6274d41b9", null ],
      [ "OddFooter", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483a4f9e6f518d4b414d5fe4960694ca7c64", null ],
      [ "EvenFooter", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483ac39ba7ad6939c0693920d51eee0620b3", null ],
      [ "FirstFooter", "_spreadsheet_update_tool_8cs.html#ab30f5f04b264935f7ac9581b8e934483a1dc0c9eb558935d42f9f9cd1eb22d559", null ]
    ] ],
    [ "HFLocation", "_spreadsheet_update_tool_8cs.html#a17dee88f159c8bcd692b2189c5cd0a7b", [
      [ "Left", "_spreadsheet_update_tool_8cs.html#a17dee88f159c8bcd692b2189c5cd0a7ba945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Center", "_spreadsheet_update_tool_8cs.html#a17dee88f159c8bcd692b2189c5cd0a7ba4f1f6016fc9f3f2353c0cc7c67b292bd", null ],
      [ "Right", "_spreadsheet_update_tool_8cs.html#a17dee88f159c8bcd692b2189c5cd0a7ba92b09c7c48c520c3c55e497875da437c", null ]
    ] ],
    [ "HFStyle", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffe", [
      [ "CurrentPageNumber", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea35d3477d5db091c77cb472098c028a25", null ],
      [ "Date", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea44749712dbec183e983dcd78a7736c41", null ],
      [ "PictureAsBackground", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea5dad3e5ce23c3048cc73a1ca92f43ffb", null ],
      [ "SheetTabName", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea1872e787b53c03215bba156eef892357", null ],
      [ "TextBold", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea0d386a3565827d67f4720e88bf7d387e", null ],
      [ "TextBoldItalic", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea048ca5b220b60ebd311af8b805573938", null ],
      [ "TextDoubleUnderline", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeac201918140832a9376a00c66c6c7ec5a", null ],
      [ "TextFontColor", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea60670c2ca2038032e256f4685d64d528", null ],
      [ "TextItalic", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea4b87d00c9357797e704976c09fc34555", null ],
      [ "TextOutlineStyle", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeac44987e44e2ddbe72467f068456c217d", null ],
      [ "TextRegular", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeacd449fed2e5b62fadff33a6329e8f5aa", null ],
      [ "TextShadow", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeac4acdba3a12febf8d37b296b640549fc", null ],
      [ "TextSingleUnderline", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeae8b24be170619b3115a677cf667b5f14", null ],
      [ "TextStrikethrough", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea56459fa1256f2bc1fe72dad47ee047b1", null ],
      [ "TextSuperscript", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea9e23a1a8e8bb6dd01db014d68b59c752", null ],
      [ "TextSubscript", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea92c157944f6a4a00f7cbe34f9005a872", null ],
      [ "Time", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeaa76d4ef5f3f6a672bbfab2865563e530", null ],
      [ "TotalPages", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeafac093ddb7dab27585ae130bc39886d8", null ],
      [ "WorkbookFileName", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffea19edad40fa3ac81a52bf7825d441877c", null ],
      [ "WorkbookFilePath", "_spreadsheet_update_tool_8cs.html#a6f75704b6346f68020cca17746cd9ffeaf9e3d95b078fa84cab31c405f4801e65", null ]
    ] ]
];