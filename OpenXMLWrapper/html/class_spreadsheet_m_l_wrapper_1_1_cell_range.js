var class_spreadsheet_m_l_wrapper_1_1_cell_range =
[
    [ "CellRange", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#a25a82153efa9223b39efb4ecd8710bce", null ],
    [ "CellRange", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#acc89fcdcb1c72fc4b30a9af77a65c642", null ],
    [ "ColumnEnd", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#adf630d61a2369d4184d7ead85ccf2d3c", null ],
    [ "ColumnNameEnd", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#aa51b7331cc933f7a9c67c2bcb60437c2", null ],
    [ "ColumnNameStart", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#ae69a7181f8489c1d78e983afa5284cee", null ],
    [ "ColumnStart", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#ac49c5049763ebdaaeba1f38da7b86ffe", null ],
    [ "RowEnd", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#aeb0860f9a04cb29a0bb2c0d4fd65f144", null ],
    [ "RowStart", "class_spreadsheet_m_l_wrapper_1_1_cell_range.html#ad6b3bb1eb1f6583e70f53d2afb690269", null ]
];